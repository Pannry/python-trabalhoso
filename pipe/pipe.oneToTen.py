from multiprocessing import Process, Pipe
# from memory_profiler import profile
# from datetime import datetime
import os

# @profile(precision=4)
def work(conn):
  # start=datetime.now()
  data = b"a" * (1 << 20) #1mb
  conn.send(data)
  conn.close()
  print('sender process:', os.getppid(),', receiver process: ', os.getpid())
  # print ('pipe:', datetime.now()-start)


# @profile(precision=4)
def main():
  # start=datetime.now()
  parent, child0 = Pipe()
  child = Process(target=work, args=(child0,))
  child.start()
  child0.close()
  parent.recv()

  parent, child1 = Pipe()
  child = Process(target=work, args=(child1,))
  child.start()
  child1.close()
  parent.recv()

  parent, child2 = Pipe()
  child = Process(target=work, args=(child2,))
  child.start()
  child2.close()
  parent.recv()

  parent, child3 = Pipe()
  child = Process(target=work, args=(child3,))
  child.start()
  child3.close()
  parent.recv()

  parent, child4 = Pipe()
  child = Process(target=work, args=(child4,))
  child.start()
  child4.close()
  parent.recv()

  parent, child5 = Pipe()
  child = Process(target=work, args=(child5,))
  child.start()
  child5.close()
  parent.recv()

  parent, child6 = Pipe()
  child = Process(target=work, args=(child6,))
  child.start()
  child6.close()
  parent.recv()

  parent, child7 = Pipe()
  child = Process(target=work, args=(child7,))
  child.start()
  child7.close()
  parent.recv()

  parent, child8 = Pipe()
  child = Process(target=work, args=(child8,))
  child.start()
  child8.close()
  parent.recv()

  parent, child9 = Pipe()
  child = Process(target=work, args=(child9,))
  child.start()
  child9.close()
  parent.recv()
  
  # print ('main:', datetime.now()-start)


if __name__ == "__main__":
  main()
