from multiprocessing import Process, Pipe
# from memory_profiler import profile
# from datetime import datetime
import os

# @profile(precision=4)
def work(conn):
  # start=datetime.now()
  data = b"a" * (1 << 20) #1mb
  conn.send(data)
  conn.close()
  print('sender process:', os.getppid(),', receiver process: ', os.getpid())
  # print ('pipe:', datetime.now()-start)


# @profile(precision=4)
def main():
  # start=datetime.now()
  parent_p, child_p = Pipe()
  child = Process(target=work, args=(child_p,))
  child.start()
  child_p.close()
  parent_p.recv()
  # print ('main:', datetime.now()-start)
  

if __name__ == "__main__":
  main()
