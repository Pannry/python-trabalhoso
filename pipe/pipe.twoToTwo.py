from multiprocessing import Process, Pipe
import sys, os

def enviar(pipe1, pipe2):
  data = b"a" * (1 << 20) #1mb
  if pipe1:
    pipe1.send([data, os.getpid()])
  if pipe2:
    pipe2.send([data, os.getpid()])


def receber(pipe3, pipe4):
  a = pipe3.recv()
  b = pipe4.recv()
  if a:
    print('sender process:', a[1],', receiver process:', os.getpid())
  if b:
    print('sender process:', b[1],', receiver process:', os.getpid())

def main():

  p13, p3 = Pipe()
  p14, p4 = Pipe()
  
  p23, p3d = Pipe()
  p24, p4d = Pipe()

  # parent 1
  process1 = Process(target=enviar, args=(p13, p14,))
  process1.start()

  # # parent 2
  process2 = Process(target=enviar, args=(p23, p24,))
  process2.start()
  
  # filho 1
  process13 = Process(target=receber, args=(p3,p3d,))
  process13.start()

  # filho 2
  process14 = Process(target=receber, args=(p4,p4d,))
  process14.start()

if __name__ == "__main__":
  main()