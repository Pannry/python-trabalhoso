import subprocess, sys, os
import mySocket, myShrdMem, myPipe
import platform

#
#   SHARED MEMORY
#
def shrdMem_1_to_1():
  myShrdMem.one_to_one()

def shrdMem_1_to_10():
  myShrdMem.one_to_ten()

def shrdMem_10_to_10():
  myShrdMem.ten_to_ten()

#
#   PIPE
#
def pipe_1_to_1():
  myPipe.one_to_one()

def pipe_1_to_10():
  myPipe.one_to_ten()

def pipe_10_to_10():
  myPipe.ten_to_ten()

#
#   SOCKET
#
def socket_1_to_1():
  mySocket.one_to_one()

def socket_1_to_10():
  mySocket.one_to_ten()

def socket_10_to_10():
  mySocket.ten_to_ten()


communications = {
  '1': shrdMem_1_to_1,          # work
  '2': shrdMem_1_to_10,         # work
  '3': shrdMem_10_to_10,        # work
  '4': pipe_1_to_1,             # work
  '5': pipe_1_to_10,            # work
  '6': pipe_10_to_10,           # work
  '7': socket_1_to_1,           # work
  '8': socket_1_to_10,          # work
  '9': socket_10_to_10          # work
}


if __name__ == "__main__":
  print('Sistema:', platform.system())
  print()
  print('1 - shrdMem 1 to 1')
  print('2 - shrdMem 1 to 10')
  print('3 - shrdMem 10 to 10')
  print('4 - pipe 1 to 1')
  print('5 - pipe 1 to 10')
  print('6 - pipe 10 to 10')
  print('7 - socket 1 to 1')
  print('8 - socket 1 to 10')
  print('9 - socket 10 to 10')
  print()
  while True:    
    a = input('digite um numero, 0 para sair: ')  
    if int(a) == 0:
      break
    func = communications [str(a)]
    func()