import subprocess, sys, os, platform
# from memory_profiler import profile

# @profile(precision = 4)
def one_to_one():
  subprocess.run("start cmd /k py socket\server.argv.py 10000", shell=True)
  subprocess.run("start cmd /k py socket\client.argv.py 10000", shell=True)


# @profile(precision = 4)
def one_to_ten():
  subprocess.run("start cmd /k py socket\server.argv.py 10000", shell=True)
  for a in range(0, 10):
    os.system('py socket\client.argv.py 10000')


# @profile(precision = 4)
def ten_to_ten():
  for a in range(10001, 10011):
    subprocess.run("start cmd /k py socket\server.argv.py " + str(a), shell=True)

  for a in range(0, 10):
    for b in range(10001, 10011):
      os.system('py socket\client.argv.py ' + str(b))
