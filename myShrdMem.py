import subprocess, sys, platform, os

def one_to_one():
    subprocess.run("start cmd /k py shrdMem\shrd.oneToOne.py", shell=True)

def one_to_ten():    
    subprocess.run("start cmd /k py shrdMem\shrd.oneToTen.py", shell=True)

def ten_to_ten():    
    subprocess.run("start cmd /k py shrdMem\shrd.tenToTen.py", shell=True)
