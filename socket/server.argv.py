import socket, sys
# from memory_profiler import profile
# from datetime import datetime


# @profile(precision = 4)
def main():
    # start=datetime.now()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', int(sys.argv[1]))
    print('starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

    sock.listen(1)

    while True:
        connection, client_address = sock.accept()
        try:
            print('connection from', client_address)

            while True:
                data = connection.recv(2**25)
                if data:
                    pass
                else:
                    break
                    
        finally:
            connection.close()
    # print ('shardMem:', datetime.now()-start)
    
    
if __name__ == '__main__':
    main()