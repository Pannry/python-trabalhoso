import socket, sys
# from memory_profiler import profile
# from datetime import datetime


# @profile(precision = 4)
def main():
    # start=datetime.now()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', int(sys.argv[1]))
    print('connecting to {} port {}'.format(*server_address))
    sock.connect(server_address)

    try:
        message = b"a" * (1 << 20) #1mb
        sock.sendall(message)

    finally:
        print('closing socket')
        sock.close()
    # print ('shardMem:', datetime.now()-start)


if __name__ == '__main__':
    main()
