from multiprocessing import Process, Lock
from multiprocessing.sharedctypes import Value, Array
# from memory_profiler import profile
# from datetime import datetime
import os


# @profile(precision = 4)
def f(a, s):
    # start = datetime.now()
    print('sender process: ', os.getpid(), ', receiver process: ', os.getppid())
    s.value = s.value
    # print('shardMem:', datetime.now()-start)


# @profile(precision = 4)
def func():
    # start = datetime.now()
    s = Array('c', b"a" * (1 << 20))  # 1mb
    p = Process(target=f, args=(1, s,))
    p.start()
    p.join()
    # print('main:', datetime.now()-start)


if __name__ == '__main__':
    func()
