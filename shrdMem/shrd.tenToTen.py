from multiprocessing import Process, Lock
from multiprocessing.sharedctypes import Value, Array
# from memory_profiler import profile
# from datetime import datetime
import os


# @profile(precision = 4)
def write(data, p):
    # start=datetime.now()
    data.value = b"a" * (1 << 20)
    p.value = os.getpid()
    # print ('send shardMem:', datetime.now()-start)


# @profile(precision = 4)
def read(
    data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
    p0, p1, p2, p3, p4, p5, p6, p7, p8, p9
):
    # start=datetime.now()
    if data0:
        print('sender process: ', p0.value,', receiver process: ', os.getpid())
        data0.value = data0.value
    if data1:
        print('sender process: ', p1.value,', receiver process: ', os.getpid())
        data1.value = data1.value
    if data2:
        print('sender process: ', p2.value,', receiver process: ', os.getpid())    
        data2.value = data2.value
    if data3:
        print('sender process: ', p3.value,', receiver process: ', os.getpid())
        data3.value = data3.value
    if data4:
        print('sender process: ', p4.value,', receiver process: ', os.getpid())
        data4.value = data4.value
    if data5:
        print('sender process: ', p5.value,', receiver process: ', os.getpid())    
        data5.value = data5.value
    if data6:
        print('sender process: ', p6.value,', receiver process: ', os.getpid())
        data6.value = data6.value
    if data7:
        print('sender process: ', p7.value,', receiver process: ', os.getpid())
        data7.value = data7.value
    if data8:
        print('sender process: ', p8.value,', receiver process: ', os.getpid())    
        data8.value = data8.value
    if data9:
        print('sender process: ', p9.value,', receiver process: ', os.getpid())    
        data9.value = data9.value
    # print ('recv shardMem:', datetime.now()-start)


# @profile(precision = 4)
def main():
    # start=datetime.now()
    data0 = Array('c', 1048576)
    data1 = Array('c', 1048576)
    data2 = Array('c', 1048576)
    data3 = Array('c', 1048576)
    data4 = Array('c', 1048576)
    data5 = Array('c', 1048576)
    data6 = Array('c', 1048576)
    data7 = Array('c', 1048576)
    data8 = Array('c', 1048576)
    data9 = Array('c', 1048576)
    p0 = Value('i', 10)
    p1 = Value('i', 10)    
    p2 = Value('i', 10)
    p3 = Value('i', 10)    
    p4 = Value('i', 10)
    p5 = Value('i', 10)    
    p6 = Value('i', 10)
    p7 = Value('i', 10)    
    p8 = Value('i', 10)
    p9 = Value('i', 10)

    # Write
    sender0 = Process(target=write, args=(data0, p0))
    sender1 = Process(target=write, args=(data1, p1))
    sender2 = Process(target=write, args=(data2, p2))
    sender3 = Process(target=write, args=(data3, p3))
    sender4 = Process(target=write, args=(data4, p4))
    sender5 = Process(target=write, args=(data5, p5))
    sender6 = Process(target=write, args=(data6, p6))
    sender7 = Process(target=write, args=(data7, p7))
    sender8 = Process(target=write, args=(data8, p8))
    sender9 = Process(target=write, args=(data9, p9))
    sender0.start()
    sender1.start()
    sender2.start()
    sender3.start()
    sender4.start()
    sender5.start()
    sender6.start()
    sender7.start()
    sender8.start()
    sender9.start()

    # Receive
    receiver0 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver1 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver2 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver3 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver4 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver5 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver6 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver7 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver8 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))

    receiver9 = Process(target=read, args=(
        data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, 
        p0, p1, p2, p3, p4, p5, p6, p7, p8, p9,
    ))
    receiver0.start()
    receiver1.start()
    receiver2.start()
    receiver3.start()
    receiver4.start()
    receiver5.start()
    receiver6.start()
    receiver7.start()
    receiver8.start()
    receiver9.start()
    # print ('main:', datetime.now()-start)


if __name__ == '__main__':
    main()
