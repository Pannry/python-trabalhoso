from multiprocessing import Process, Lock
from multiprocessing.sharedctypes import Value, Array
# from memory_profiler import profile
# from datetime import datetime
import os


# @profile(precision = 4)
def newProcess(s):
  # start=datetime.now()
  print('sender process: ', os.getppid(), ', receiver process: ', os.getpid())
  s.value = s.value
  # print ('shardMem:', datetime.now()-start)


# @profile(precision = 4)
def main():
  # start = datetime.now()
  s = Array('c', b"a" * (1 << 20))  # 1mb
  for a in range(1, 11):
    p = Process(target=newProcess, args=(s,))
    p.start()
    p.join()
  # print ('main:', datetime.now()-start)


if __name__ == '__main__':
  main()
