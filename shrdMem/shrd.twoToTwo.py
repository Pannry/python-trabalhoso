from multiprocessing import Process, Lock
from multiprocessing.sharedctypes import Value, Array
import os

def write(data, p):
    data.value = b"a" * (1 << 20)
    p.value = os.getpid()

def read(data0, data1, p0, p1):
    if data0:
        print(p0.value, os.getpid())
        data0.value = data0.value
    if data1:
        print(p1.value, os.getpid())
        data1.value = data1.value

def main():
    data0 = Array('c', 1048576)
    data1 = Array('c', 1048576)
    p0 = Value('i', 10)
    p1 = Value('i', 10)

    # Write
    sender0 = Process(target=write, args=(data0, p0))
    sender1 = Process(target=write, args=(data1, p1))
    sender0.start()
    sender1.start()

    # Receive
    receiver0 = Process(target=read, args=(data0, data1, p0, p1,))
    receiver1 = Process(target=read, args=(data0, data1, p0, p1,))
    receiver0.start()
    receiver1.start()


if __name__ == '__main__':
    main()
